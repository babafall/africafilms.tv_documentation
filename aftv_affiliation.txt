============================================================================
TITLE: A PROPOSAL OF HOW TO ADD THE AFFILIATION FEATURE ON THE AFTV PLATFORM
============================================================================


todo sample:


1. create the mysql tables:
  - africafilms_affiliate: who are the affiliates we are dealing with 
  - africafilms_widget: what are the existing widgets
  - africafilms_refered_client: the client who has bought a movie, coming from an affiliate
  - africafilms_affiliate_widget: what are the widgets corresponding to a known affiliate
  - africafilms_affiliate_earning: money made by affiliate
  - africafilms_widget_template: template of widget
  - africafilms_content: ...	

2. create php classes inside aftv/library/Lomitko: 
  - TDG = Table Data Gateway, a php class access to the "table" level 
  - RDG = Row Data Gateway, a php class access to the "row" level, inside TDG there is 0, 1 or many RDG
	- AffiliateTDG.php + directory containing AffiliateRDG.php
	- WidgetTDG.php + directory containing WidgetRDG.php
	- ReferededClientTDG.php + directory containing ReferedClientRDG.php
	- AffiliateWidgetTDG.php + directory containing AffiliateWidgetRDG.php
	- AffiliateEarningTDG.php + directory containing AffiliateEarningRDG.php
	- WidgetTDG.php + WidgetRDG.php
	- WidgetTemplateTDG.php + WidgetTemplateRDG.php

The client table is already existing: africafilms_client
The media table is already existing: africafilms_media
To find the earning, we should check: africafilms_mvt table

3. Maybe we can create new controllers called AffiliationController.php and WidgetController.php inside the application/controller directory. Then add some basic CRUD operations inside.

4. Most boring part, create the different related views (html pages)



step by step logic sample:


- for every new request, inspect the $_SERVER variable, it can provide valuable informations about where the request came from.
- extract the keys we need
- check if the couple affiliate/widget exists in our db
- if yes, store in the zend registry the phpsessid, the affiliate_id, ...
- for every new sell? compare the phpsessid, the id of the sold item, to the ones stored inside the zend registry
- modify the split
