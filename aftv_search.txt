TITLE: SMALL DESCRIPTION OF HOW THE SEARCH IS DONE
--------------------------------------------------
AUTHOR: "patrick nsukami"
-------------------------
DATE: february 5th 2013, 22:02
------------------------------
VERSION: 1


- if you're a user looking for a movie, call to /application/controllers/FilmController.php, the action is rechercheAction.
- The methods getFilms/getFilm/getFilmBack, in the php class library/Lomitko/FilmsFront.php are used to retrieve a movie depending on various parameters 
- the table where the search is done is africafilms_search a table containing lots of field to avoid writing joins.

this is an example of a function retrieving one movie, the main parameter here are $pays_id to check if the movie is available in the country of the user currently doing the query

  public function getFilm($id_film, $lang, $pays_id, $mode = NULL)
  {
    if (empty($pays_id)) {
      throw new Exception("Il faut un pays pour localiser la liste des films disponibles.");
    }
    if (!in_array($lang, array("fr","en","es","pt"))) throw new Exception("La lang fournie est vide ou ne fait pas partie des langues du site.");
    $select = $this->select()->where(" africafilms_search.id = $id_film ")
      ->joinInner(array("ava" => "africafilms_availaible"), "ava.film_id = africafilms_search.id ")
      ->joinInner(array("fil" => "africafilms_film"), "fil.id = africafilms_search.id ")
      ->joinLeft(array("med" => "africafilms_media"), "med.id = fil.ba_" . $lang . "_id ", array("caption_ba" => "caption_" . $lang, "videoFileName" => "fileName"));
    if (!is_null($mode))
      $select->where("ava.mode = ?", $mode)->where(" ava.pays_id = $pays_id ")->where(" africafilms_search.pays = $pays_id ");
    //die($select);
    return $this->fetchRow($select);
  }


The $pays_id is coming most of the time from the geolocation system. You are geolocated using /application/plugins/Lomitko/Geoloc.php. 
And, depending on where you are geolocated, you'll see the available movie on that region. 
To know what are the movies available on your region, we'll check africafilms_availaible table

The $pays_id can also come from a dropdown list, example when a user is doing a query research.
