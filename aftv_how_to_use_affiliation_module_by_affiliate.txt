===================================================
TITLE: HOW TO USE AFFILITION MODULE BY AN AFFILIATE
===================================================
:Authors:
         Papa Baba Fall

:Version:
         1.1
:Date:
        april 11th 2014, 15:59

1. How an affiliate connects to his account
-------------------------------------------

First time, we are on the linding page of africafilms.tv.
We scroll to the footer to find affiliation link. See below, link colored yellow.

.. figure::  images/aftv_footer.png
   :align:   center

When we click on the llink , we get the page below.
Enter login : <baba>, and password : <passer>.

.. figure:: images/aftv_aff_log_in.png
   :align:  center

When we click on button CONNEXION, we arrived on home page of affiliation module. At first we can see a list of all widgets created by an affiliate. See below.

.. figure::  images/aftv_aff_default_page.PNG
   :align:   center

This page shows four blocs:
- North bloc displays affiliate name, current hour and a list of theme.
- West bloc displays menu.  
  + The link “DETAILS” is activated by default. It enables to show list of widgets.  
  + The link “CREATE WIDGET” displays all movies. See below.

.. figure::  images/aftv_cw_menu.PNG
   :align:   center

  + The link "MVT WIDGET" enables to list all mouvements of split from buy or rent movies of clients coming from his website. See below

.. figure::  images/aftv_aff_mvt.PNG
   :align:   center



2. Manage affiliates
 --------------------

An affiliate is a kind of user that can post movies trailors on their website. They can be created and managed from this interface that we see below.

.. figure:: images/back_users_aff.PNG
   :align:  center

