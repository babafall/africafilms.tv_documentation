TITLE: SMALL DESCRIPTION OF THE MAIN DIRECTORIES/FILES OF THE AFTV PLATFORM
--------------------------------------------------------------------------------------
AUTHOR: "patrick nsukami"
-------------------------
DATE: december 19th 2012, 13:06
--------------------------------
VERSION: 1
----------


AFTV PROJECT TREE:
------------------

- africafilms.tv/application:
-----------------------------
  contains all the configuration files, the most important are:
  - the application.ini
  - the routes.ini
  - the lang/ directory

- africafilms.tv/application/Bootstrap.php:
-------------------------------------------
  a really important file, all the initialization stuff goes there.

- africafilms.tv/library, contains:
-----------------------------------
  - php files about pdf generation
  - zend library
  - lomitko library (where all the TDG/RDG classes are)
  - pear library

- africafilms.tv/htdocs:
------------------------
  contains all the stuff used in the front, html/css/js/
  there is also some public resources: invoices, swf player, ...

  the entry point should be index.phpbut it is index2.php (don't know what's the deal)
  the GeoIP.dat seems to be important, a compiled file containing all the coutries mapped to IP

- africafilms.tv/movies:
------------------------
  contains all the movie files

- africafilms.tv/pluginLoaderCache.php:
---------------------------------------
  seems to be a kind of cache for the plugins loader by the platform.
  don't know the use of this file, I only know that it should be writable, so I chmod 777


- africafilms.tv/application/modules:
-------------------------------------
  an attempt to divide the application in modules.
  until now, the most used modules were default (frontend) and back (backend) modules.
  never been to the others.

- africafilms.tv/application/modules/back:
------------------------------------------
  the back office application:
  - extjs for the front: /views/scripts
  - php/zend for the logic: /controllers
