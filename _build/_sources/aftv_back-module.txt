TITLE: DESCRIPTION OF THE BACK MODULE
----------------------------------------------------------------------------------------------------------
AUTHOR: "papa baba fall"
-------------------------
DATE: april 5th 2013, 11:12
----------------------------
VERSION: 1
----------

The module back is about management of the back office. Means, it's from where contents are added, removed or edited.

The back module has 24 controllers : 

ActiviteController.php     CompteController.php     RegieController.php
AmbassadeurController.php  DiaporamaController.php  RepertoireController.php
AuthController.php         FilmController.php       TaxeController.php
AyantdroitController.php   IndexController.php      TpltarifController.php
BlogController.php         LangueController.php     TypologieController.php
ClientController.php       MailboxController.php    UsersController.php
ClubController.php         NewsController.php       ZoneController.php
CollecteurController.php   PaysController.php

-------------------------------------
: 1- IndexController.php controller :
-------------------------------------
It has 2 classes :
    - MyFilterOperator : class that extends FilterIterator. It's used to filter list of files from a directory and render only php files
    - IndexController : class that controlles movies, listings, and all editing tabs. 
    It contents 15 actions listed below :
        + init() :
        Get params from request and call init action of parent class
        
        + apcAction() :
        List all "php" files contened in Zend library 
        
        + timeAction() :
        Display time at format day/month/year hour:minute
        
        + importzoneAction() :
        Add countries for GeoIp from csv file
        
        + indexAction() : 
        Redirect only to ext action
        
        + venteAction() :
        Does nothing
        
        + cacheflushAction() :
        Clear front cache
        
        + refreshgeoipAction() : 
        Update Geoip countries by downloading the recent zip file from site.
        
        + activateAction() : 
        Update movies database from a SQL file 
        
        NB : Need to be changed ! -> use php_mysql function and param to connect to SGBD 
              """ 
               $link = mysql_connect("localhost", "mobicine", "6580124102354")
			   or die("Impossible de se connecter : " . mysql_error()); 
			  """
			  
        + onlineAction() : 
        ? Purhaps an action about session
        
        + onlinetomorrowAction() : 
        ? Purhaps an action about session
        
        + venteexectuteAction() :
        Valide selling movie
        
        + extAction() : 
        Get params that be rendered to principal view for back module ("ext.phtml").
        It's the main action among all actions standed into back module.
        
        + listepaysAction() : 
        Return the list of countries in JSON format for combobox 
        
        + listetarifsAction() :
        Action to retrieve the list of templates rates in JSON format
        
The most important action is ext, and his view is the main template for all back office.
All view render JSON data to "ext.phtml". 
And it's to this view to display data into his content area.

ext.phtml -> Africafilms.tv_dev/application/modules/back/views/scripts/index/ext.phtml
---------------------------------------------------------------------------------------
Only this view stand for something. 
The view is divided into four parts:

   - North: the header of the page. It contains the name of the administrator, a button "LOG OUT" and a selection list "Choose Theme"
   
   - West: navigation block. It contains two main menus:
     -> CATALOGUE: FILMS, PEOPLE, USERS, BLOG, MEDIA, DATA, MAIL, DIAPORAMA, CLIENTS, ACTIVITE, REGIE 
     -> ADMIN: Compte Africafilms, Compte Mobicine, Listing des mouvements, Préparation mise à jour du catalogue en ligne, 
     Pousser la mise à jour du catalogue en ligne, Invalider le cache
     
   - Center: content block. Whenever a listing should appear, it is at this level.
   
   - South: block footer.
   
   
   
---------------------------------------
: 2-AuthController.php controller :
---------------------------------------
Controller authentication management. 
This controller has four actions :
    + init() : get zend authentication instance, desable the layout and create a session nammed 
    "AfricafilmsTvBackAdmin"
    
    + indexAction() : it returns to the main page if user is logged in else user is redirect to the login page. Means
    teste if user is well logged to redirect him to index/ext view, else to loggin page.
    
    + logoutAction() : user logoff.  The session nammed "AfricafilmsTvBackAdmin" is completely destroyed as to destroy acls.
    unset session variable 'AfricafilmsTvBackAdmin' stored and redirect to login page (back/auth/login).
    
    + loginAction() : display a connexion form if user doesn't send any data otherwise store authentication 
    objet if data sended by user are valid and redirect to controller index and action ext.
    It does nothing if request isn't post or parameters login and password are get parameters.
    Else create an authentication object and checks if user is valid. If it's true, check if his online parameter is 
    change to true. If it's the case, stores some data into session variable before renders the user to back/index/ext.
    
There is only "auth.phtml" for views of authController. It displayes a basic html form. That permit user to tape their 
login and password.


   
   
--------------------------------------------
:  3- AyantdroitController.php Controller  :
--------------------------------------------
Controller ayant_droit management. This controller has heigt actions : 
    
    + init() : calles init of class parent
    
    + extAction() : any code at the action but its view page render JS for the page to administartion ayantdroits.
    
    + extlistAction() : returns the list of ayantdroits paged into JSON format
    
    + autocompleteAction() : returns the list of ayantdroits for combo box in autocomplete
    
    + passwordAction() : allows you to change the password of an ayantdroit
    
    + addextAction() : allows to add an ayantdroit. If successed, send an email to ayantdroit.
    
    + deleteAction() : allows to delete an ayantdroit
    
    + statsAction() : generate statistics for ayantdroit and save its datas
    
For corresponding views, they all work with "ext.phtml" view page. 
They send JSON format data after their process to confirm successful issu or 
failed else to give content that will be displayed into main page into "ext.phtml" file.
    

--------------------------------------------
: 4- AmbassadeurController.php Controller  :
--------------------------------------------

It is the same Controller as AyantdroitController. There are the same actions. 


---------------------------------------------
:  5- CollecteurController.php  Controller  :
---------------------------------------------

Controller management collection organization. There is ten actions : 

    + init() : call init of class parent.
    
    + indexAction() : there is no code here. And there is no view corresponding to action index.
    
    + filmAction() : idem. Can be erased
    
    + extAction() : idem. Can be erased.
    
    + extlistAction() : render the list of collector in JSON format. 
    The main action for CollecteurController. It checks Collectors that respect 
  the filter parameter passed by an user to render them into JSON format to ext.phtml view file.
  At the view level there is a JS function that renders a Ext.lomitko.LomitkoGridUser witch extends 
  Ext.grid.GridPanel, ExtJS class.
    
    + autocompleteAction() : render the list of collector in JSON format for combobox.
    
    + searchAction() : displayes countries matched to search param directely. It doesn't use a view page and layout.
    
    + passwordAction() : allows to change password.
    
    + addextAction() : allows to add or edit a new collector.
    
    + deleteAction() : allows to delete a collecteur.
    
In the view/script/collecteur directory there is only "ext.phtml" file. 
The only view page is ext.phtml. It has all content in a variable type panel that will be recovered 
by ext::index-view. 


--------------------------------------
: 6-UsersController.php  Controller  :
--------------------------------------

Controller management users. Their is height actions :

    + init() : call to parent init action. 
    
    + extAction() : initiates a new zend config ini and passes it to its view variable.
    
    + extlistAction() : render the users list in JSON format.
    
    + autocompleteAction : render the list of user in JSON format for combobox.
    
    + passwordAction() : allows to change a user password.
    
    + addextAction() : allows to add a new user.
    
    + deleteAction() : allows to delete a user.
    
For corresponding views, they all work with "ext.phtml" view page. They send JSON format data after their process to confirm successful issu or failed else to give content that will be displayed into main page into "ext.phtml" file.



-----------------------------------------
:7- ActiviteController.php  Controller  :      
-----------------------------------------

Controller management ayantdroit. There is twelve actions : 

    + init() : call to parent init action. 
    
    + extAction() : there is no code. But its view (ext.phtml), render JSON data for administration of activities.
    
    + autocompleteAction(): render the list of user in JSON format for combobox.  
    
    + activiteAction() : check if the client with id x, is already recorded.
    
    + cancelAction() :  write detail of a cancelled transaction on a the /logs/mvt_transactions_cancel.log file.
    
    + paiementAction() : render list of payment with their corresponding message transaction in JSON format.
    
    + factureAction() : render list of bills with their corresponding message transaction in JSON format.
    
    + achatAction() : render list of sales with their corresponding message transaction in JSON format.
    
    + telechargementAction() : render list of downloads with their corresponding message transaction in JSON format.
    
    + facturehtmlAction() : display bills into html format.
    
    + invalidAction() : invalid a download.
    
    + validAction() : valid a download.
    


-----------------------------------
:8-BlogController.php Controller  :
-----------------------------------

Controller Management Bonus movie. There is five actions :

    + init() : call to parent init action.
    
    + indexAction() : renders the movie object, concerned by id got from param request to their index view. Into file "index.phtml", there is an ExtJS function that get this information to list bonus of the movie.
    
    + extlistAction() : return the list of bonus movie in JSON format.
    
    + editAction() : allows to edit a bonus movie.
    
    + deleteAction() : allows to delete a bonus movie.
    
    + addextAction() : allows to add a bonus.
    
    
Into view directory of BlogController there is two files:

    + index.phtml that render list of bonus movie in a panel object
    
    + ext.phtml that render also detail of bonus of a movie
    


---------------------------------------
:9- ClientController.php Controller   :
---------------------------------------
    
Controller Management Client. There are eleven actions : 

    + init() : call to parent init action.
    
    + extAction() : No codes. But its view page renders the administration panel to manage clients.
    
    + extlistAction() : Returns the expanded list of beneficiaries into the JSON format.
    
    + autocompleteAction(): Return the list of beneficiaries  in JSON format for combobox.
    
    + passwordAction(): allows customer to change his password
    
    + addextAction(): allows to add a new customer
    
    + deleteAction(): allows to delete a customer
    
    + statsAction(): generate statistics about ambassador. Gives a list of movie for an ayantdtoit 
    
    + activiteAction(): allows to display activities panels
    
    + paiementAction(): allows to display paiements activities  
    
    + achatAction(): allows to display purshases activities 
    
    + telechargementAction(): allows to display downloads activities 
    
    + newsletterAction(): allows to display a table list of customer

    
    
    
---------------------------------------
:10-ClubController.php Controller     :
---------------------------------------

This Controller isn't yet in use. There is no folder view corresponding to those actions.
It has seven actions:

  
  + init(): calls parent action
  
  + extAction(): nothing. 
  
  + extaddformAction(): nothing.
  
  + extlistAction(): 
  
  + deleteAction(): 
  
  + saveficheAction(): 
  
  + recapAction():

Seem not be in use.  
  
-----------------------------------------
: 11-  CompteController.php controller  :
-----------------------------------------
Controller for management of account. There are twelve actions : 

  + init(): call parent init action.
  
  + indexAction(): allows to display financial transaction about account of all kind customers.
  
  + pdfAction(): allows to display financial transaction account into a PDF format.
  
  + transactionAction(): displays detail about account of all kind of customers.
  
  + factureAction(): allows to make transaction and to record a bill.
  
  + quarterpdfAction(): Returns the balance on a quarter into PDF format.
  
  + bordereauAction(): Edit slip bill at the last stop date.
  
  + encourspdfAction(): Returns a PDF with the country of operation recap on a movie from the date of last arrest
  
  + titulairesAction(): Renders number of holders movie.
  
  + mvtAction(): allows to valid a financial movement to a customer.
  

  
  
--------------------------------------------
: 12-  DiaporamaController.php controller  :
--------------------------------------------  
Controller highlighted movies. There are five actions: 

  + init(): calls parents actions.
  
  + indexAction(): nothing. But there is index.phtml that corresponds to index action.
  It list the highlighted movies that will be display at home page of africafilms.tv
  
  + extlistAction(): returns a list of heighlighted movies into JSON format.
  
  + addextAction(): allows to add a highlighted movie.
  
  + deleteAction(): allows to delete a highlighted movie.





---------------------------------------
: 13-  FilmController.php controller  :
---------------------------------------  

Controller Movies, listings, and editing all tabs. There are thirty seven (37) actions:

  + init(): idem
  
  + extAction(): Renders JS client listing movies
  
  + extaddformAction(): Loads form to add, edit movie.
  
  + saveficheAction(): Allows to save movie sheet in database.
  
  + saveversionAction(): Allows to save movie version in database.
  
  + saveequipeAction(): back panel of the film team in the database and return a dataset,
  in the recovery values ​​used to display the correpsondant id to initialize the form.
  
  + savemediaAction(): save panel of digital movie in database.
  
  + savetarifAction(): save panel tarif movie in database.
  
  + savetaxeAction(): save recurring tag.
  
  + savepaysAction(): save recurring country.
  
  + saveayantdroitAction(): allows to save an ayantdroit.
  
  + copyadAction(): allow to copy setup of an ayantdroit.
  
  + savediffusionAction(): allows to save recurring broadcasting country of a movie.
  
  + savebandeannonceAction(): allows to save trailer of movie.
  
  + recapAction(): Allows to load summury of a movie.
  
  + loadbandeannonceAction(): Allows to load the contents of panel trailer.
  
  + loadversionAction(): Allows to load the contents of panel version.
  
  + loadequipeAction(): Allows to load the contents of panel team
  
  + loadmediaAction(): Allows to load the contents of panel digital movie.
  
  + loadtarifAction():
  
  + loadtaxeAction(): 
  
  + loadayantdroitAction(): 
  
  + loadpaysAction(): 
  
  + loaddiffusionAction(): 
  
  + extlistAction():
  
  + extlistautocompleteAction():
  
  + copypanelAction():
  
  + listeAction():
  
  + deleteAction():
  
  + statutAction():
  
  + pushonlineAction():
  
  + ficheAction():
  
  + ficheAction():
  
  + savefichiersAction():
  
  + loadfichiersAction():
  
  + reportingAction(): so long.


---------------------------------------------
: 14-  LangueController.php controller  :
---------------------------------------------
There are six actions :


---------------------------------------------
: 15-  MailboxController.php controller  :
---------------------------------------------
There are nine actions :

 


---------------------------------------------
: 16-  NewsController.php controller  :
--------------------------------------------- 
There are seven actions :


---------------------------------------------
: 17-  PaysController.php controller  :
--------------------------------------------- 
There are six actions :

---------------------------------------------
: 18-  RegieController.php controller  :
---------------------------------------------
There are eleven actions :


---------------------------------------------
: 19-  RepertoireController.php controller  :
--------------------------------------------- 
There are nine actions :


---------------------------------------------
: 20-  TaxeController.php controller  :
---------------------------------------------  
There are six actions :


---------------------------------------------
: 21-  TpltarifController.php controller  :
--------------------------------------------- 
There are six actions :


---------------------------------------------
: 22-  TypologieController.php controller  :
--------------------------------------------- 
 There are six actions :
 

---------------------------------------------
: 23-  ZoneController.php controller  :
--------------------------------------------- 
There are height (8) actions :