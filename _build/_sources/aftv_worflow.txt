TITLE: AN EXAMPLE OF HOW TO ADD A NEW FEATURE ON THE AFTV PLATFORM
-------------------------------------------------------------------
AUTHOR: "patrick nsukami"
-------------------------
DATE: december 19th 2012, 13:06
--------------------------------
VERSION: 1
----------


A WAY TO ADD NEW FEATURES:
--------------------------
- Example: return the movies download list for a know aftv client.
- Zend is an MVC framework, so:

- MODEL: WHAT DATAS ARE YOU DEALING WITH ?
------------------------------------
1. what are the tables involved? add tables if needed
   - africafilms_paiement, to check only for the ok payments
   - africafilms_achat, to find the movies ids bought buy the client
   - africafilms_film, to find informations about the movies
   - africafilms_client_telechargement, to know the already downloaded movie files
   - africafilms_film_fichier, to know what are the files associated to a movie
   - africafilms_film_audio, to know the lang of a movie file
   - africafilms_film_st, to know the subtitle of a movie file
   - africafilms_langue, to know what are the available lang

2. what are the php classes representing the data model involved? add corresponding classes if you added tables
   - path: africafilms.tv/library/Lomitko
   - Paiements.php a table data gateway to the africafilms_paiement table
   - Either there is no TDG for the africafilms_achat, or there is and name is not explicit
   - Film.php a TDG to the africafilms_film table
   - Telechargements.php the TDG to the africafilms_client_telechargement table
   - FilmFichiers.php a TDG to the africafilms_film_fichier table
   - FilmsFichiers/Audios.php TDG to the africafilms_film_audio
   - FilmsFichiers/STs.php TDG to the africafilms_film_st
   - Langue.php TDG africafilms_langue

- CONTROLLER: WHAT LOGIC ?
--------------------------
3. Every urls are mapped to actions, actions exists inside controllers. So the next, thing is:
   modify the existing router, to add new routes.
   router path: africafilms.tv/application/config/routes.ini or routes_fr.ini, routes_en.ini, etc ...

   add the new route: define the type, the name, the module, the controller, the action to execute
   routes.clientdownloads.type = "Zend_Controller_Router_Route"
   routes.clientdownloads.route = "clientdownloads"
   routes.clientdownloads.defaults.module = "default"
   routes.clientdownloads.defaults.controller = "inscription"
   routes.clientdownloads.defaults.action = "clientdownloads"

4. I have found an action inside the Inscription controller which was doing a similar work.
   It would have been better to create a new controller for this task, (maybe the next step)
   path: africafilms.tv/application/controller/ (where all the front controllers are)

   add an action: public function clientdownloadAction inside InscriptionController.php
   - write code.

- VIEW: WHERE TO RETURN RESULTS ?
---------------------------------
5. No need to write html for this feature, thx. to return data just use the die() function.

   But if it was necessary to write an html view:
   the file containing the main layout of aftv: africafilms.tv/application/layouts/scripts/layout.phtml
   all the main view parts are build using:
   - tpl files inside africafilms.tv/application/layouts/scripts/partials/
   - php helper classes inside africafilms.tv/application/views/helpers/
   - html files inside africafilms.tv/application/views/scripts/ (organized inside directories)
