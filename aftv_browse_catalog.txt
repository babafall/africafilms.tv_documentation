TITLE: SMALL DESCRIPTION OF THE CATALOG BROWSING INSIDE AFTV
------------------------------------------------------------
AUTHOR: "patrick nsukami"
-------------------------
DATE: jully 05th 2013, 03:46 
----------------------------
VERSION: 1
----------

PHP classes involved in the catalog browsing, no need to login:
----------------------------------------------------------------

*** The geolocation:
--------------------

There is a dedicated plugin ***Geoloc.php*** used to geolocate the user and to display content in function
This plugin is called with each requests coming to the server

module:     default
controller: GeolocController (used only to retrieve the countries, not sure if still used)
action:     index
model:      Lomitko_Pays
table:      africafilms_pays


*** The search engine:
----------------------

module: default
controller: FilmController
action: rechercheAction
model: Lomitko_FilmsFront
table: africafilms_search


*** The channels:
-----------------
Used to show movies, by categories: series, films, documentaries

module:     default
controller: ChaineController
action:     indexAction
model:      Lomitko_FilmsFront
table:      africafilms_search


*** The cast:
-------------
Used to show informations about directors, actors, ...

module: default
controller: PersonneController
action: indexAction
model: Lomitko_Repertoire / Lomitko_Typologie
table: africafilms_repertoire / africafilms_typologie
